# Tom Robert 
>  Fullstack developer 

## Contact

Email: tom.robert@iamroot.io

Linkedin: https://www.linkedin.com/in/tomrobert/
## Technology 
* Javascript 
    * Vanilla
    * Typescript
    * Angular 
    * NodeJS (express,  Nest.js) 
    * Cypress
*  PHP
    *  Laravel 
    *  Drupal/Symphony
* SQL Database 
    * MySql 
    * Postresql
*  HTML/CSS 
*  Devops 
    *  Docker 
    *  Gitlab CI 
    *  Ansible 

## Skills
* Techlead 
* Solutions architect 
* Enterprise REST design 
* Senior Developer 

## Experience 

* Lampiris/Total ( 5y)
    * Websites b2b/b2c (drupal)
    * my.lampiris.be (Angular) 
    * services.lampiris.be (Angular + nest.js)
    * Online registration/simulation (Angular)
* Dazzle/Desk02 (5y) 
*  TCS (8m)
*  Corelio (1y)
